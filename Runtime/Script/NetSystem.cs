using UnityEngine;
using UnityEngine.UI;
using VRC.SDK3.Components;
using VRC.SDK3.Data;
using VRC.SDK3.Image;
using VRC.SDKBase;
using VRC.Udon;
using VRC.Udon.Common;
using VRC.Udon.Common.Interfaces;
using UdonSharp;


namespace dev.ebi9.udoncard
{
    [UdonBehaviourSyncMode(BehaviourSyncMode.None)]
    public class NetSystem :  UdonSharpBehaviour
    {
        //public int i = 0;
        NetChannnel[] chans = new NetChannnel[0]; // 消息通道列表 

        NetChannnel chan =>
            (chans.Length == 0 ? (chans = GetComponentsInChildren<NetChannnel>()) : chans)[EX.localpid() % chans.Length];

        TweenV3 tw;

        public DataToken GetEventParam = new DataToken(); // 消息同步参数 

        private void Start()
        { 
            EX.init(nameof(TweenV3), ref tw);
        }

        public void SendNetworkEvent(Component self, string method, DataToken param)
            => chan.SendNetworkEvent(self.name, method, param);

        public void flushAt(float f) => chan.flushAt(f);

        public void setGameObject(GameObject x)
        {
            if (x.GetComponent<RectTransform>()) return;
            var p = x.transform.position;
            var r = x.transform.rotation.eulerAngles;
            var param = new DataList();
            param.Add(x.name);
            param.Add(p.x);
            param.Add(p.y);
            param.Add(p.z);
            param.Add(r.x);
            param.Add(r.y);
            param.Add(r.z);
            param.Add(x.transform.parent.name);
            param.Add(x.transform.GetKinematic());
            chan.SendNetworkEvent(nameof(sg), param);
        }

        public void sg()
        {
            var parma = GetEventParam.DataList;
            var g = GameObject.Find(((string)parma[0]));
            if (g == null) return;
            var t = g.transform;
            t.position = new Vector3(
                (float)parma[1].Number,
                (float)parma[2].Number,
                (float)parma[3].Number
            );
            t.rotation = Quaternion.Euler(new Vector3(
                (float)parma[4].Number,
                (float)parma[5].Number,
                (float)parma[6].Number
            ));
            t.parent = GameObject.Find(((string)parma[7])).transform;
            t.SetKinematic(((bool)parma[8]));
            t.SetTrigger(((bool)parma[8]));
        }

        public void setLocalPosition(string name, Vector3 x, float tweenDt = 0)
        {
            var param = new DataList();
            param.Add(name);
            param.Add(x.x);
            param.Add(x.y);
            param.Add(x.z);
            param.Add(tweenDt);
            chan.SendNetworkEvent(nameof(sl), param);
        }

        public void sl()
        {
            var parma = GetEventParam.DataList;
            var t = GameObject.Find(((string)parma[0])).transform;
            var tt = (float)parma[4].Number;
            if (tt == 0)
            {
                t.localPosition = new Vector3(
                    (float)parma[1].Number,
                    (float)parma[2].Number,
                    (float)parma[3].Number
                );
            }
            else
            {
                tw.TweenMoveLocal(t, new Vector3(
                    (float)parma[1].Number,
                    (float)parma[2].Number,
                    (float)parma[3].Number
                ), tt);
            }
        }

        public void setLocalTransform(string name, Transform t, float tweenDt = 0)
        {
            var p = t.localPosition;
            var r = t.localRotation.eulerAngles;
            setLocalTransform(name, p, r, tweenDt);
        }
        public void setLocalTransform(string name, Vector3 p, Vector3 r, float tweenDt = 0)
        {
            var param = new DataList();
            param.Add(name);
            param.Add(p.x);
            param.Add(p.y);
            param.Add(p.z);
            param.Add(r.x);
            param.Add(r.y);
            param.Add(r.z);
            param.Add(tweenDt);
            chan.SendNetworkEvent(nameof(st), param);
        }

        public void st()
        {
            var parma = GetEventParam.DataList;
            var tf = GameObject.Find(((string)parma[0])).transform;
            var tt = (float)parma[7].Number;
            if (tt == 0)
            {
                tf.localPosition = new Vector3(
                    (float)parma[1].Number,
                    (float)parma[2].Number,
                    (float)parma[3].Number
                );
                tf.localRotation = Quaternion.Euler(new Vector3(
                    (float)parma[4].Number,
                    (float)parma[5].Number,
                    (float)parma[6].Number
                ));
            }
            else
            {
                tw.TweenMoveLocal(tf, new Vector3(
                    (float)parma[1].Number,
                    (float)parma[2].Number,
                    (float)parma[3].Number
                ), tt);
                tw.TweenRotateLocal(tf, Quaternion.Euler(new Vector3(
                    (float)parma[4].Number,
                    (float)parma[5].Number,
                    (float)parma[6].Number
                )), tt);
            }
        }

        public void setTransform(string name, Transform t, float tweenDt = 0)
        {
            var p = t.position;
            var r = t.rotation.eulerAngles;
            setTransform(name, p, r, tweenDt);
        }
        public void setTransform(string name, Vector3 p, Vector3 r, float tweenDt = 0)
        {
            var param = new DataList();
            param.Add(name);
            param.Add(p.x);
            param.Add(p.y);
            param.Add(p.z);
            param.Add(r.x);
            param.Add(r.y);
            param.Add(r.z);
            param.Add(tweenDt);
            chan.SendNetworkEvent(nameof(tt), param);
        }

        public void tt()
        {
            var parma = GetEventParam.DataList;
            var tf = GameObject.Find(((string)parma[0])).transform;
            var tt = (float)parma[7].Number;
            if (tt == 0)
            {
                tf.position = new Vector3(
                    (float)parma[1].Number,
                    (float)parma[2].Number,
                    (float)parma[3].Number
                );
                tf.rotation = Quaternion.Euler(new Vector3(
                    (float)parma[4].Number,
                    (float)parma[5].Number,
                    (float)parma[6].Number
                ));
            }
            else
            {
                tw.TweenMove(tf, new Vector3(
                    (float)parma[1].Number,
                    (float)parma[2].Number,
                    (float)parma[3].Number
                ), tt);
                tw.TweenRotate(tf, Quaternion.Euler(new Vector3(
                    (float)parma[4].Number,
                    (float)parma[5].Number,
                    (float)parma[6].Number
                )), tt);
            }
        }

        public void setParent(string name, string parent_name)
        {
            var param = new DataList();
            param.Add(name);
            param.Add(parent_name);
            chan.SendNetworkEvent(nameof(sp), param);
        }

        public void sp()
        {
            var parma = GetEventParam.DataList;
            var t = GameObject.Find(((string)parma[0])).transform;
            var p = GameObject.Find(((string)parma[1])).transform;
            t.parent = p;
        }

        public void setStatic(string name, bool dy)
        {
            var param = new DataList();
            param.Add(name);
            param.Add(dy);
            chan.SendNetworkEvent(nameof(ss), param);
        }

        public void ss()
        {
            var parma = GetEventParam.DataList;
            var t = GameObject.Find(((string)parma[0])).transform;
            t.SetKinematic(((bool)parma[1]));
            t.SetTrigger(((bool)parma[1]));
        }

    }
}