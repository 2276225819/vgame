using UdonSharp;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using VRC.SDK3.Components;
using VRC.SDK3.Data;
using VRC.SDK3.Image;
using VRC.SDKBase;
using VRC.Udon;
using VRC.Udon.Common;
using VRC.Udon.Common.Interfaces;

namespace dev.ebi9.udoncard
{
    public class LabelAttribute : PropertyAttribute
    {
        public readonly string label;
        public LabelAttribute(string tooltip) { label = tooltip; }
    }


    [UdonBehaviourSyncMode(BehaviourSyncMode.Manual)]
    public class PerfabLoader : UdonNetSharpBehaviour
    {

        private NetSystem Net; //同步对象 

        [Header(" [ Image ] ")]

        [UdonSynced, FieldChangeCallback(nameof(syncUrlImage))]
        public VRCUrl CardDataUrl;
        public VRCUrl syncUrlImage
        {
            set
            {
                FrontImage = BackImage = null;
                loadurlhash(CardDataUrl = value);
            }
        }
        public Texture2D FrontImage;
        public Texture2D BackImage;
        public Texture2D LoadingIcon; // 
        public Texture2D ErrorIcon; // 
        public Texture2D HideIcon; // 

        [Header(" [ Loader ] ")]

        public GameObject Prefab; //   
        public VRCUrlInputField InputField;
        public Color[] colors = new Color[0]; // 颜色序列 空格分割 TryParseHtmlString 
        [Label("index")] public float i; // 开始序号
        [Label("scale")] public float s; // 世界缩放比例

        [Header(" [ Front Image Attribute ] ")]

        [Label("number")] public float n; // 总数量      
        [Label("column")] public float l; // 平铺数量
        [Label("left")] public float x; // 图片位置左
        [Label("top")] public float y; // 图片位置上  
        [Label("right")] public float r; // 平铺边距右
        [Label("bottom")] public float d; // 平铺边距下  
        [Label("width")] public float w; // 图片尺寸宽   
        [Label("height")] public float h; // 图片尺寸高  
        [Label("sizew")] public float u; // 纹理尺寸宽  
        [Label("sizeh")] public float v; // 纹理尺寸高

        [Header(" [ Back Image Attribute ] ")]
        [Label("number")] public float N; // 总数量      
        [Label("column")] public float L; // 平铺数量
        [Label("left")] public float X; // 图片位置左
        [Label("top")] public float Y; // 图片位置上  
        [Label("right")] public float R; // 平铺边距右
        [Label("bottom")] public float D; // 平铺边距下  
        [Label("width")] public float W; // 图片尺寸宽   
        [Label("height")] public float H; // 图片尺寸高  
        [Label("sizew")] public float U; // 纹理尺寸宽  
        [Label("sizeh")] public float V; // 纹理尺寸高 

        [HideInInspector]
        public string[] lx = new string[0];

        bool localImg = false;
        private void Start()
        {
            localImg = BackImage != null;
            EX.init(nameof(NetSystem), ref Net);
            OnReload();
        }


        public override void OnPlayerJoined(VRCPlayerApi player)
        {
            // 非房主不发送
            if (!Networking.IsMaster)
                return;

            // 房主不接收
            if (Networking.Master.playerId == player.playerId)
                return;

            // 房主发送全量同步消息
            OnResync();
        }

        void OnDrawGizmos()
        {
            // 在 Scene 视图中绘制立方体
            if (Prefab == null) return;
            if (colors != null && colors.Length > 0) Gizmos.color = colors[0];

            // 保存当前矩阵
            Matrix4x4 oldMatrix = Gizmos.matrix;

            var p = ss(transform.localScale);
            Gizmos.matrix = Matrix4x4.TRS(transform.position, Prefab.transform.rotation * transform.rotation, p);
            var q = Prefab.GetComponentInChildren<MeshFilter>();
            if (q)
            {
                Gizmos.DrawWireMesh(q.sharedMesh, Vector3.zero);
                Gizmos.DrawRay(Vector3.up * q.sharedMesh.bounds.size.y * 0.5f, Vector3.forward * 0.03f);
            }


            // 恢复之前的矩阵
            Gizmos.matrix = oldMatrix;
        }


        Vector3 ss(Vector3 p)
        {
            var s = this.s == 0 ? 1 : this.s; // s = 1;
            var w = this.w == 0 ? 1 : this.w; // s = 1;
            var h = this.h == 0 ? 1 : this.h; // s = 1;
            var z = (s * 1.4142135623730951f) / Mathf.Sqrt(w * w + h * h);
            p.x = w * z;
            p.y = h * z;
            p.z = s;
            return p;
        }

        public void ApplyMaterial(int i, Material ms1, Material ms2)
        {
            var pw = 0f; //正反面共用
            var ph = 0f; //正反面共用
            var ow = 1f; //正反面共用
            var oh = 1f; //正反面共用
            var vn = 1f; //正反面共用

            // 正面图片
            if (FrontImage != null) // && (x != 0 || y != 0 || w != 0 || h != 0))
            {
                var mw = (float)FrontImage.width;
                var mh = (float)FrontImage.height;
                var bx = x;
                var by = y;
                if (r != 0) pw = r;
                if (d != 0) ph = d;
                if (w != 0) ow = w;
                if (h != 0) oh = h;
                if (u != 0) mw = u;
                if (v != 0) mh = v;
                if (n != 0) vn = n;
                if (l != 0)
                {
                    var page = i % vn;
                    bx += Mathf.Floor(page % l) * (ow + pw); //偏移
                    by += Mathf.Floor(page / l) * (oh + ph); //偏移
                }

                if (ms1 != null)
                {
                    ms1.mainTexture = FrontImage;
                    ms1.mainTextureOffset = new Vector2(bx / mw, -(by / mh) - (oh / mh));
                    ms1.mainTextureScale = new Vector2(ow / mw, oh / mh);
                }
            }

            // 背面图片
            if (BackImage != null) // && (X != 0 || Y != 0 || W != 0 || H != 0))
            {
                var mw = (float)BackImage.width;
                var mh = (float)BackImage.height;
                var bx = X;
                var by = Y;
                if (R != 0) pw = R;
                if (D != 0) ph = D;
                if (W != 0) ow = W;
                if (H != 0) oh = H;
                if (U != 0) mw = U;
                if (V != 0) mh = V;
                if (N != 0) vn = N;
                if (L != 0)
                {
                    var page = i % vn;
                    bx += Mathf.Floor(page % L) * (ow + pw); //偏移
                    by += Mathf.Floor(page / L) * (oh + ph); //偏移 
                }

                if (ms2 != null)
                {
                    ms2.mainTexture = BackImage;
                    ms2.mainTextureOffset = new Vector2(bx / mw, -(by / mh) - (oh / mh));
                    ms2.mainTextureScale = new Vector2(ow / mw, oh / mh);
                }
            }


            // 颜色数量 数字或具体颜色值
            if (ms1 != null && colors.Length != 0)
            {
                var ii = i % colors.Length;
                ms1.color = colors[ii];
            }
        }

        const int imgf = 0;
        const int imgb = 1;

        public void ApplyImage(Image rn1, int page)
        {
            if (FrontImage == null) return;
            var tt = FrontImage;
            var s = tt.width / u;
            var bx = (int)x;
            var by = (int)y;
            if (l != 0 && n != 0)
            {
                bx += (int)(Mathf.Floor(page % l) * (r + w)); //偏移
                by += (int)(Mathf.Floor(page / l) * (d + h)); //偏移
            }

            var rr = (new Rect(bx * s, (v - by - h) * s, w * s, h * s));
            if (rr.x + rr.width > tt.width) return;
            if (rr.y + rr.height > tt.height) return;
            var ss = Sprite.Create(FrontImage, rr, Vector2.zero);
            ss.name = $"{page}  = {bx} {by} {w} {h}   :{s} ";
            rn1.sprite = ss;
        }

        public void ApplyProps(GameObject obj, int i, bool test = false)
        {
            // 图像
            var rn = obj.GetComponentInChildren<Renderer>();
            if (rn != null && !test)
            {
                var ms = rn.materials;
                ApplyMaterial(i,
                    ms.Length > imgf ? ms[imgf] : null,
                    ms.Length > imgb ? ms[imgb] : null);
            }

            var rn1 = obj.GetComponentInChildren<Image>();
            if (rn1 != null && !test)
            {
                ApplyImage(rn1, i);
            }

            // 缩放比例 (正面图片)
            if (rn != null)
            {
                obj.transform.localScale = ss(obj.transform.localScale);
            }

            if (rn1 != null)
            {
                obj.transform.localScale = Vector3.one;
            }
        }

        public void ApplyIcon(Texture t)
        {
            for (int i = 0; i < loaded.Count; i++)
            {
                var obj = (GameObject)loaded[i].Reference;
                var rn = obj.GetComponentInChildren<Renderer>();
                if (rn != null)
                {
                    var ms = rn.materials[imgf];
                    ms.mainTexture = t;
                    ms.mainTextureOffset = Vector2.zero;
                    ms.mainTextureScale = Vector2.one;
                }
            }
        }

        public void SetProps(string lx)
        {
            ClsProps();
            var hash = lx.toDic();
            var hash_key = hash.GetKeys();
            for (int i = 0; i < hash.Count; i++)
            {
                var k = ((string)hash_key[i]);
                if (float.TryParse((string)hash[k], out var v))
                    SetProgramVariable(k, v);
            }
        }
        public void ClsProps()
        {
            var u = this;
            u.x = u.X = u.y = u.Y = u.w = u.W = u.h = u.H = 0;
            u.r = u.R = u.d = u.D = u.u = u.U = u.v = u.V = 0;
            u.l = u.L = u.i = u.n = u.s = 0;
        }

        public bool loadurlhash(VRCUrl UrlImage, bool nodown = false)
        {
            var urlhash = UrlImage.ToString();
            if (!urlhash.tryParseUrl(out string url, out lx))
            {
                return false;
            }

            if (InputField)
            {
                InputField.SetUrl(UrlImage);
            }

            // 立即创建所有预制体
            for (int group = 0; group < lx.Length; group++)
            {
                SetProps(lx[group]);
                loadlGameobject(group);
            }

            // 下载图片 更新正面背面精灵
            if (!nodown && url.StartsWith("http") && FrontImage == null && BackImage == null)
            {
                ApplyIcon(LoadingIcon);
                var _imageDownloader = new VRCImageDownloader();
                _imageDownloader.DownloadImage(UrlImage, null, (IUdonEventReceiver)this, null);
            }

            return true;
        }

        public override void OnImageLoadSuccess(IVRCImageDownload result)
        {
            base.OnImageLoadSuccess(result);
            var rs = result.Result;
            var img = Sprite.Create(rs, new Rect(0, 0, rs.width, rs.height), Vector2.one * 0.5f);
            FrontImage = BackImage = img.texture;
            u = U = img.rect.width;
            v = V = img.rect.height;

            for (int i = 0; i < loaded.Count; i++)
            {
                var obj = (GameObject)loaded[i].Reference;
                var ss = obj.name.Split(new char[] { '/' }, 3);
                if (!int.TryParse(ss[1], out int group)) continue;
                if (!int.TryParse(ss[2], out int ii)) continue;
                SetProps(lx[group]);
                ApplyProps(obj, ii);
            }
            lx = new string[0];
        }

        public override void OnImageLoadError(IVRCImageDownload result)
        {
            base.OnImageLoadError(result);
            Debug.LogError("[OnImageLoadError]" + result.ErrorMessage);
            ApplyIcon(ErrorIcon);
        }


        DataList loaded = new DataList(); // <GameObject>  


        public const float 卡牌高度 = 0.003f;
        public void loadlGameobject(int group)
        {
            if (group == 0) OnClear();

            // 创建新预制体 
            for (int i = (int)this.i; i < n; i++)
            {
                var obj = Instantiate(Prefab);
                obj.name = $"{name}/{group}/{i}";
                obj.transform.parent = transform;
                obj.transform.position = transform.position + (Vector3.up * 卡牌高度 * (1 + i) * 2);
                obj.transform.rotation = transform.rotation * Prefab.transform.localRotation;
                obj.SetActive(true);
                loaded.Add(obj);
                ApplyProps(obj, i);
            }
        }

        public void Reload()
        {
            if (InputField && InputField.GetUrl().Get() != ""
                && CardDataUrl.ToString() != InputField.GetUrl().ToString())
            {
                if (!this.RequestSerialization(nameof(Reload))) return;

                syncUrlImage = InputField.GetUrl();
            }
            else
            {
                this.SendCustomNetworkEvent(NetworkEventTarget.All, nameof(OnReload));
            }
        }
        public void ReloadAll()
        {
            var ls = transform.parent.GetComponentsInChildren<PerfabLoader>();
            for (int i = 0; i < ls.Length; i++)
            {
                ls[i].Reload();
            }
        }

        public void OnReload()
        {
            // 如果是网络地址 而且未加载完成 
            if (CardDataUrl != null && CardDataUrl.ToString() != "" && !localImg)
            {
                loadurlhash(CardDataUrl); // 加载网络地全部参数 
            }
            else
            {
                loadlGameobject(0);   // 加载预定义参数预制体 
            }
        }
        public void OnClear()
        {
            // 重新加载默认预制体
            if (loaded.Count != 0)
            {
                for (int i = 0; i < loaded.Count; i++)
                {
                    var x = (GameObject)loaded[i].Reference;
                    Destroy(x);
                }

                loaded.Clear();
            }
        }

        public void OnResync()
        {
            for (int i = 0; i < loaded.Count; i++)
                Net.setGameObject((GameObject)loaded[i].Reference);
        }
    }
}