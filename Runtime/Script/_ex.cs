
using System.Collections.Generic;
using System.Globalization;
using UdonSharp;
using UnityEngine;
using UnityEngine.UI;
using VRC.SDK3.Components;
using VRC.SDK3.Data;
using VRC.SDK3.Image;
using VRC.SDKBase;
using VRC.Udon;
using VRC.Udon.Common;
using VRC.Udon.Common.Interfaces;
using static VRC.SDKBase.VRCPlayerApi;


namespace dev.ebi9.udoncard
{
    public static class EX
    {
        public static T[] append<T>(this T[] a, T b)
        {
            var urls = new T[a.Length + 1];
            for (int i = 0; i < urls.Length - 1; i++)
            {
                urls[i] = a[i];
            }
            urls[urls.Length - 1] = b;
            return urls;
        }
        public static T[] grow<T>(this T[] a, int b)
        {
            var urls = new T[b];
            for (int i = 0; i < b && i < a.Length; i++)
            {
                urls[i] = a[i];
            }
            return urls;
        }

        public static void init<T>(string name, ref T tmp) where T : Component
        {
            var iname = name + "(Clone)";
            var obj = GameObject.Find(iname);
            if (obj == null)
            {
                obj = GameObject.Find(name);
                if (obj == null)
                {
                    var c = tmp != null && tmp.gameObject.name == name;
                    obj = c ? GameObject.Instantiate(tmp.gameObject) : null;
                    if (obj == null)
                    {
                        Debug.LogError($"ERRRRRRRRRRRRRRRRRR:  {iname}");
                    }
                }
                obj.name = iname;
            }
            tmp = obj.GetComponent<T>();
        }


        public static string getVal(this string s, string k)
        {
            tryKeyStr(s, k, out string ret);
            return ret;
        }
        public static bool tryParseUrl(this string s, out string url, out string[] ls)
        {
            url = null;
            ls = null;
            var ss = s.Split(new char[] { '#' }, 2);
            if (ss.Length != 2) return false;
            url = ss[0];
            var sss = ss[1].Split('&', System.StringSplitOptions.RemoveEmptyEntries);
            if (sss.Length == 0) return false;
            ls = new string[sss.Length];
            for (int i = 0; i < sss.Length; i++)
            {
                ls[i] = sss[i];
            }
            return true;
        }
        //public static string I => ";";
        public static string toHash(params string[] s)
        {
            return string.Join(";", s);
        }

        public static bool tryKeyStr(this string s, string k, out string v)
        {
            v = "";
            var ss = s.StartsWith(k) ? 0 : s.IndexOf(k = $";{k}");
            if (ss == -1) return false;
            ss += k.Length;
            var ee = s.IndexOf(";", ss);
            v = ee == -1 ? s.Substring(ss) : s.Substring(ss, ee - ss);
            return true;
        }
        public static bool tryKeyInt(this string s, string k, out int v)
        {
            v = 0;
            if (!s.tryKeyStr(k, out string vv)) return false;
            return int.TryParse(vv, out v);
        }

        public static bool tryKeyInt(this DataDictionary s, string k, out int v)
        {
            v = 0;
            if (!s.TryGetValue(k, out DataToken vv)) return false;
            return int.TryParse(vv.String, out v);
        }
        public static bool tryKeyStr(this DataDictionary s, string k, out string v)
        {
            v = null;
            if (!s.TryGetValue(k, out DataToken vv)) return false;
            v = vv.String;
            return true;
        }
        public static bool tryKeyFloat(this DataDictionary s, string k, out float v)
        {
            v = 0;
            if (!s.TryGetValue(k, out DataToken vv)) return false;
            return float.TryParse(vv.String, out v);
        }

        public static int getKeyInt(this DataDictionary s, string k)
        {
            s.tryKeyInt(k, out int i);
            return i;
        }
        public static float getKeyFloat(this DataDictionary s, string k)
        {
            s.tryKeyFloat(k, out float i);
            return i;
        }

        public static DataDictionary toDic(this string s)
        {
            var ss = s.Split(';', System.StringSplitOptions.RemoveEmptyEntries);
            var dic = new DataDictionary();
            for (int i = 0; i < ss.Length; i++)
            {
                var kk = ss[i][0].ToString();
                dic[kk] = ss[i].Substring(1);
            }
            return dic;
        }


        public static bool TryParseHtmlString(string htmlString, out Color color)
        {
            color = Color.white;
            if (htmlString.Length <= 1) return false;
            if (htmlString[0] == '#')
            {
                if (htmlString.Length == 4)//123
                {
                    byte r = byte.Parse(htmlString.Substring(1, 1), NumberStyles.HexNumber);
                    byte g = byte.Parse(htmlString.Substring(2, 1), NumberStyles.HexNumber);
                    byte b = byte.Parse(htmlString.Substring(3, 1), NumberStyles.HexNumber);
                    color = new Color((r * 16 + r) / 255f, (g * 16 + g) / 255f, (b * 16 + b) / 255f, 1);
                    return true;
                }
                if (htmlString.Length == 5)//1234
                {
                    byte r = byte.Parse(htmlString.Substring(1, 1), NumberStyles.HexNumber);
                    byte g = byte.Parse(htmlString.Substring(2, 1), NumberStyles.HexNumber);
                    byte b = byte.Parse(htmlString.Substring(3, 1), NumberStyles.HexNumber);
                    byte a = byte.Parse(htmlString.Substring(4, 1), NumberStyles.HexNumber);
                    color = new Color((r * 16 + r) / 255f, (g * 16 + g) / 255f, (b * 16 + b) / 255f, (a * 16 + a) / 255f);
                    return true;
                }
                if (htmlString.Length == 7)//112233
                {
                    byte r = byte.Parse(htmlString.Substring(1, 2), NumberStyles.HexNumber);
                    byte g = byte.Parse(htmlString.Substring(3, 2), NumberStyles.HexNumber);
                    byte b = byte.Parse(htmlString.Substring(5, 2), NumberStyles.HexNumber);
                    color = new Color(r / 255f, g / 255f, b / 255f, 1);
                    return true;
                }
                if (htmlString.Length == 9)//11223344
                {
                    byte r = byte.Parse(htmlString.Substring(1, 2), NumberStyles.HexNumber);
                    byte g = byte.Parse(htmlString.Substring(3, 2), NumberStyles.HexNumber);
                    byte b = byte.Parse(htmlString.Substring(5, 2), NumberStyles.HexNumber);
                    byte a = byte.Parse(htmlString.Substring(4, 1), NumberStyles.HexNumber);
                    color = new Color(r / 255f, g / 255f, b / 255f, a / 255f);
                    return true;
                }
            }
            else
            {
                switch (htmlString.ToLower())
                {
                    case "maroon": color = new Color(0.5f, 0, 0); return true;
                    case "brown": color = new Color(0.64f, 0.16f, 0.16f); return true;
                    case "tan": color = new Color(0.82f, 0.7f, 0.54f); return true;
                    case "orange": color = new Color(1f, 0.64f, 0); return true;
                    case "peach": color = new Color(1f, 0.85f, 0.72f); return true;
                    case "gold": color = new Color(1f, 0.84f, 0); return true;
                    case "lime": color = new Color(0, 1, 0); return true;
                    case "green": color = new Color(0, 0.5f, 0); ; return true;
                    case "olive": color = new Color(0.5f, 0.5f, 0f); ; return true;
                    case "teal": color = new Color(0f, 0.5f, 0.5f); ; return true;
                    case "cyan": color = new Color(0f, 1f, 1f); ; return true;
                    case "purple": color = new Color(0.5f, 0f, 0.5f); ; return true;
                    case "pink": color = new Color(1f, 0.7f, 0.7f); ; return true;
                    case "yellow": color = Color.yellow; return true; ;
                    case "grey": color = Color.grey; return true;
                    case "gray": color = Color.gray; return true;
                    case "magenta": color = Color.magenta; break;
                    //case "cyan": color = Color.cyan; return true;
                    case "red": color = Color.red; return true;
                    case "black": color = Color.black; return true;
                    case "white": color = Color.white; return true;
                    case "blue": color = Color.blue; return true;
                        //case "green": color = Color.green; return true;
                }
            }
            return false;
        }
        public static void SetColliders(this Component g, bool b)//, bool tt = false)
        {
            var xx = g.GetComponents<Collider>();
            for (int i = 0; i < xx.Length; i++)
            {
                xx[i].enabled = b;
                //xx[i].isTrigger = tt;
            }
        }
        public static void SetTrigger(this Component g, bool tt = false)
        {
            var xx = g.GetComponents<Collider>();
            for (int i = 0; i < xx.Length; i++)
            {
                xx[i].isTrigger = tt;
            }
        }
        public static void SetKinematic(this Component g, bool b)
        {
            var gg = g.GetComponent<Rigidbody>();
            if (!gg) return;
            gg.isKinematic = b;
        }

        public static bool GetKinematic(this Component g)
        {
            var gg = g.GetComponent<Rigidbody>();
            if (!gg) return false;
            return gg.isKinematic;
        }

        public static void SendCustomEvent(this Component g, string s)
        {
            g.GetComponent<UdonBehaviour>().SendCustomEvent(s);
        }

        //public static bool Raycast<T>(string s, Vector3 tpos, Vector3 rota, out T g, out Vector3 p)
        //{
        //    g = default(T);
        //    p = Vector3.zero;
        //    if (Physics.Raycast(tpos, rota, out var hit, 9999, LayerMask.GetMask(s)))
        //    {
        //        if (hit.collider == null) return false;
        //        g = hit.collider.GetComponent<T>();
        //        p = hit.point;
        //        return g != null;
        //    }
        //    return false;
        //}
        public static bool Raycast(int mask, Vector3 tpos, Vector3 rota, float v, out RaycastHit p)
        {
            p = new RaycastHit();
            //var ls Physics.RaycastAll(tpos, rota, 999, LayerMask.GetMask(s));
            //Networking.LocalPlayer.GetAvatarEyeHeightAsMeters();

            if (Physics.Raycast(tpos, rota, out var hit, v, mask))
            {
                p = hit;
                if (hit.collider == null) return false;
                var g = hit.collider;
                if (g.transform == null) return false;
                if (g.GetComponentInParent<VRCPickup>()) return false; // 排除 canvas
                //if (g.GetComponent<PickupV3>() != null) return false; // 排除 canvas
                return g != null;
            }
            return false;
        }
        public static bool Raycast(string mask, Vector3 tpos, Vector3 rota, float v, out RaycastHit p)
        {
            return Raycast(LayerMask.GetMask(mask), tpos, rota, v, out p);
        }

        public static bool ismobile()
        {
            return (EX.isandroid() && !EX.isvr());
        }
        public static bool isandroid()
        {
#if UNITY_ANDROID
            return true;
#else
            return false;
#endif
        }
        public static bool isvr()
        {
            if (Networking.LocalPlayer == null) return false;
            return Networking.LocalPlayer.IsUserInVR();
        }
        public static int localpid()
        {
            if (Networking.LocalPlayer == null) return 0;
            return Networking.LocalPlayer.playerId;
        }

        public static float holdwait() => 0.4f;// isandroid() ? 1f : 0.5f;

        public static bool local(out VRCPlayerApi p)
        {
            p = Networking.LocalPlayer;
            return p != null;
        }

        public static bool GetPlayerById(int i, out VRCPlayerApi pid)
        {
            pid = VRCPlayerApi.GetPlayerById(i);
            return pid != null && pid.IsValid();
        }

        public static bool lookVec(this VRCPlayerApi p, TrackingDataType mainhand, ref Vector3 tpos, ref Vector3 rota, out Quaternion q)
        {
            if (p != null)
                switch (mainhand)
                {
                    case TrackingDataType.LeftHand:
                        var leftHand = p.GetTrackingData(TrackingDataType.LeftHand);
                        tpos = leftHand.position;
                        rota = leftHand.rotation * Vector3.Lerp(Vector3.right, Vector3.forward, isandroid() ? 0.5f : 0.54f) * 0.2f;
                        q = leftHand.rotation * Quaternion.Euler(0, 90, 90); ;
                        return true;
                    case TrackingDataType.RightHand:
                        var rightHand = p.GetTrackingData(TrackingDataType.RightHand);
                        tpos = rightHand.position;
                        rota = rightHand.rotation * Vector3.Lerp(Vector3.right, Vector3.forward, isandroid() ? 0.5f : 0.54f) * 0.2f;
                        q = rightHand.rotation * Quaternion.Euler(0, 90, 90);
                        return true;
                    case TrackingDataType.Head:
                        var Head = p.GetTrackingData(TrackingDataType.Head);
                        tpos = Head.position;// + Head.rotation * Vector3.back;
                        rota = Head.rotation * Vector3.forward * Mathf.Min(0.4f, p.GetAvatarEyeHeightAsMeters() * 0.8f);
                        q = Head.rotation;
                        return true;
                    default: break; // v*1.8  - v*1
                }
            rota = tpos = Vector3.zero;
            q = Quaternion.identity;
            return false;
        }


        public static State kstate(float v, ref float holdtime, ref int holdkey)
        {
            if (v > 0.8f)
            {
                if (holdtime == 0)
                {
                    //holdkey = key;
                    holdtime = Time.realtimeSinceStartup;
                    return State.down;
                }
                if (holdtime != 0)
                {
                    var t2 = Time.realtimeSinceStartup - holdtime;
                    if (t2 > holdwait())
                    {
                        return State.holding;
                    }
                    else
                    {
                        return State.ready;
                    }
                }
            }
            else
            {
                if (holdtime != 0)
                {
                    var t2 = Time.realtimeSinceStartup - holdtime;
                    holdtime = 0;
                    if (t2 > holdwait())
                    {
                        return State.hold;
                    }
                    else
                    {
                        return State.click;
                    }
                }
                else if (holdkey != NClick)
                {
                    holdkey = NClick;
                }
            }
            return State.none;
        }
        public static void kvalue(TrackingDataType mainhand, ref float v, ref int holdkey)
        {
            if (holdkey == NClick)
            {
                //v = kdown(mainhand, RClick);
                //if (v != 0)
                //{
                //    holdkey = RClick;
                //    return;
                //}
                v = kdown(mainhand, LClick);
                if (v != 0)
                {
                    holdkey = LClick;
                    return;
                }
                v = kdown(mainhand, MClick);
                if (v != 0)
                {
                    holdkey = MClick;
                    return;
                }
                v = kdown(mainhand, UClick);
                if (v != 0)
                {
                    holdkey = UClick;
                    return;
                }
                v = kdown(mainhand, DClick);
                if (v != 0)
                {
                    holdkey = DClick;
                    return;
                }
                v = 0;
            }
            else
            {
                v = kdown(mainhand, holdkey);
            }
        }
        //public static bool Vook(TrackingDataType mainhand, ref int holdkey)
        //{
        //    if (Networking.LocalPlayer == null) return false;
        //    var v = 0f;
        //    if (TrackingDataType.RightHand == mainhand)
        //        v = Input.GetAxis("Oculus_CrossPlatform_SecondaryThumbstickVertical");
        //    if (TrackingDataType.Head == mainhand)
        //        v = Input.GetAxis("Mouse ScrollWheel");
        //    if (v > 0)
        //    {
        //        holdkey = UClick;
        //    }
        //    if (v < 0)
        //    {
        //        holdkey = DClick;
        //    }
        //    return v != 0;// v > 0 ? State.up : State.down;  
        //}
        public static float kdown(TrackingDataType mainhand, int ckey)
        {
            // https://docs.google.com/spreadsheets/d/1_iF0NjJniTnQn-knCjb5nLh6rlLfW_QKM19wtSW_S9w/edit#gid=1150012376 
            switch (mainhand)
            {
                case TrackingDataType.LeftHand:
                    switch (ckey)
                    {
                        case RClick: return Input.GetAxis("Oculus_CrossPlatform_PrimaryHandTrigger") > 0.8f ? 1 : 0;
                        case LClick: return Input.GetAxis("Oculus_CrossPlatform_PrimaryIndexTrigger") > 0.8f ? 1 : 0;
                    }
                    break;
                case TrackingDataType.RightHand:
                    switch (ckey)
                    {
                        //case RClick: return Input.GetAxis("Oculus_CrossPlatform_SecondaryHandTrigger") > 0.8f ? 1 : 0;
                        case LClick: return Input.GetAxis("Oculus_CrossPlatform_SecondaryIndexTrigger") > 0.8f ? 1 : 0;
                        case UClick: return Input.GetAxis("Oculus_CrossPlatform_SecondaryThumbstickVertical") > 0 ? 1 : 0;
                        case DClick: return Input.GetAxis("Oculus_CrossPlatform_SecondaryThumbstickVertical") < 0 ? 1 : 0;
                    }
                    break;
                case TrackingDataType.Head:
                    if (ckey == UClick) return Input.GetAxis("Mouse ScrollWheel") > 0 ? 1 : 0;
                    if (ckey == DClick) return Input.GetAxis("Mouse ScrollWheel") < 0 ? 1 : 0;
                    if (ckey > 2) return 0;
                    return Input.GetMouseButton(ckey) ? 1 : 0;// require VRCSDK <= 3.5 
            }
            return 0;
        }

        public static bool tryGetComponent<T>(this Component g, out T obj)
        {
            obj = default(T);
            if (g == null) return false;
            obj = g.GetComponent<T>();
            return obj != null;
        }
        public static bool tryGetComponent<T>(this GameObject g, out T obj)
        {
            obj = default(T);
            if (g == null) return false;
            obj = g.GetComponent<T>();
            return obj != null;
        }
        public static void InitComponent<T>(this Component g, ref T obj)
        {
            if (obj == null)
            {
                obj = g.GetComponent<T>();
            }
        }


        public static Vector3 RoundAngles(this Vector3 up)
        {
            up.x = Mathf.Round(up.x / 90f) * 90f;
            up.y = Mathf.Round(up.y / 90f) * 90f;
            up.z = Mathf.Round(up.z / 90f) * 90f;
            return up;
        }

        public static Quaternion RoundAngles(this Quaternion up)
        {
            return Quaternion.Euler(up.eulerAngles.RoundAngles());
        }
        public static Vector3 RoundVAngles(this Vector3 up)
        {
            up.x = Mathf.Round(up.x / 180f) * 180f;
            up.y = Mathf.Round(up.y / 90f) * 90f;
            up.z = Mathf.Round(up.z / 180f) * 180f;
            return up;
        }

        public static Quaternion RoundVAngles(this Quaternion up)
        {
            return Quaternion.Euler(up.eulerAngles.RoundVAngles());
        }
        public static Quaternion RoundYAngles(this Quaternion originalRotation)
        {
            Vector3 eulerAngles = originalRotation.eulerAngles; // 提取欧拉角
            eulerAngles.x = 0; // 锁定Y轴的旋转角度为0
            eulerAngles.z = 0; // 锁定Y轴的旋转角度为0
            return Quaternion.Euler(eulerAngles); // 将欧拉角转换回四元数 
        }

        //public static Vector3 RoundHAngles(this Vector3 up)
        //{
        //    up.x = Mathf.Round(up.x / 90f) * 90f;
        //    up.y = Mathf.Round(up.y / 90f) * 90f;
        //    up.z = Mathf.Round(up.z / 90f) * 90f;
        //    return up;
        //}

        //public static Quaternion RoundHAngles(this Quaternion up)
        //{
        //    return Quaternion.Euler(up.eulerAngles.RoundHAngles());
        //}

        // 获取 GameObject 的完整路径
        public static string GetFullPath(this GameObject gameObject)
        {
            string path = "/" + gameObject.name;
            Transform current = gameObject.transform.parent;

            while (current != null)
            {
                path = "/" + current.name + path;
                current = current.parent;
            }

            return path;
        }
        public static bool tryFind(string name, out GameObject f)
        {
            f = GameObject.Find(name);
            return f != null;
        }

        public static bool getTmp(this DataDictionary alltype, string value, out GameObject v)
        {
            if (value.tryKeyStr("t", out string type) && alltype.ContainsKey(type))
            {
                v = (GameObject)alltype[type].Reference;
                return true;
            }
            v = null;
            return false;
        }
        public static bool getKeyPos(string value, out Vector3 t, out Quaternion q)
        {
            t = Vector3.zero;
            q = Quaternion.identity;
            if (value.tryKeyStr("T", out var type))
            {
                var pos = type.Split(',');
                if (pos.Length >= 3)
                {
                    t.x = float.Parse(pos[0]);
                    t.y = float.Parse(pos[1]);
                    t.z = float.Parse(pos[2]);
                    if (pos.Length >= 6)
                        q = Quaternion.Euler(
                         float.Parse(pos[3]),
                         float.Parse(pos[4]),
                         float.Parse(pos[5])
                        );
                    return true;
                }
            }
            return false;
        }
        public static void GetBoneData(this VRCPlayerApi player, int currentHand, out Vector3 x, out Quaternion z)
        {
            var type = (currentHand == 1 ? HumanBodyBones.LeftHand : HumanBodyBones.RightHand);
            x = player.GetBonePosition(type);
            z = player.GetBoneRotation(type);

            //var type = (VRCPlayerApi.TrackingDataType)(currentHand);
            //var tt = player.GetTrackingData(type);
            //z = tt.rotation;
            //x = tt.position;
        }

        public const int LClick = 0;
        public const int RClick = 1;
        public const int MClick = 2;
        public const int UClick = 21;
        public const int DClick = 22;
        public const int NClick = 999;

    }

    public class UdonNetSharpBehaviour : UdonSharpBehaviour
    {
        string method = "";
        bool req = false;
        public override void OnOwnershipTransferred(VRCPlayerApi player) => this.syncdone();

        public bool RequestSerialization(string domethod, float t)
        {
            if (method == "")
            {
                method = domethod;
                req = true;
                if (Networking.IsOwner(this.gameObject))
                    SendCustomEventDelayedSeconds(nameof(syncdone), t);
                else if (EX.local(out var p))
                    Networking.SetOwner(p, this.gameObject);
                return false;
            }
            return true;
        }
        public bool RequestSerialization(string domethod, bool doreq = true)
        {
            if (method == "")
            {
                method = domethod;
                req = doreq;
                if (Networking.IsOwner(this.gameObject))
                    SendCustomEventDelayedFrames(nameof(syncdone), 1);
                else if (EX.local(out var p))
                    Networking.SetOwner(p, this.gameObject);
                return false;
            }
            return true;
        }
        public void syncdone()
        {
            if (method != "")
            {
                SendCustomEvent(method);
                method = "";
                if (req)
                {
                    RequestSerialization();
                    OnDeserialization();
                }
            }
        }

        public override void OnDeserialization()
        {
            base.OnDeserialization();
        }
    }


    public enum State
    {
        none, down, up, ready, holding, click, hold
    }

}