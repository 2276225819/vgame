
using UdonSharp;
using UnityEngine;
using UnityEngine.UI;
using VRC.SDK3.Components;
using VRC.SDK3.Data;
using VRC.SDK3.Image;
using VRC.SDKBase;
using VRC.Udon;
using VRC.Udon.Common;
using VRC.Udon.Common.Interfaces;

namespace dev.ebi9.udoncard
{
    [UdonBehaviourSyncMode(BehaviourSyncMode.Manual)]
    public class NetChannnel : UdonNetSharpBehaviour
    {
        [UdonSynced]
        public string sync_txt = "";
        [System.NonSerialized]
        UdonBehaviour parent = null;
        [System.NonSerialized]
        DataList queue_msg = new DataList();// 消息同步回调
        float processing = 0;//消息合并中标记
        private void Start()
            => parent = transform.parent.GetComponent<UdonBehaviour>();
        public void SendNetworkEvent(string domethod, DataToken doparam)
             => SendNetworkEvent("", domethod, doparam);

        public void SendNetworkEvent(string target, string domethod, DataToken doparam)
        {
            var param = new DataList();
            param.Add(domethod);
            param.Add(doparam);
            if (target != "") param.Add(target);
            queue_msg.Add(param);
            if (processing == 0)
            {
                processing = 0.01f;
                RequestSerialization(nameof(StartSerialization));
            }
        }
        public void flushAt(float t)
        {
            if (processing == 0)
            {
                RequestSerialization(nameof(StartSerialization), t);
            }
            processing = t;

        }


        public void StartSerialization()
        {
            if (queue_msg.Count == 0) return;
            VRCJson.TrySerializeToJson(queue_msg,
                JsonExportType.Minify, out DataToken txt);
            sync_txt = txt.ToString();
            queue_msg.Clear();
        }

        public override void OnDeserialization()
        {
            base.OnDeserialization();
            VRCJson.TryDeserializeFromJson(sync_txt, out DataToken param);
            var lss = param.DataList;
            //Debug.LogError("RECEIVE: " + lss.Count);
            for (int i = 0; i < lss.Count; i++)
            {
                var mp = lss[i].DataList;
                parent.SetProgramVariable(nameof(NetSystem.GetEventParam), mp[1]);
                var t = parent;
                if (mp.Count == 3)
                    t = GameObject.Find(((string)mp[2])).GetComponent<UdonBehaviour>();
                if (t != null)
                    t.SendCustomEvent(((string)mp[0]));
            }
            SendCustomEventDelayedSeconds(nameof(OnDeserialization2), 0.3f);
        }
        public void OnDeserialization2()
        {
            if (queue_msg.Count == 0)
            {
                processing = 0;
            }
            else
            {
                RequestSerialization(nameof(StartSerialization), processing);
            }
        }
    }

}