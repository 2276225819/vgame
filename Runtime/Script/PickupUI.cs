
using UdonSharp;
using UnityEngine;
using UnityEngine.UI;
using VRC.SDKBase;
using VRC.Udon;
namespace dev.ebi9.udoncard
{

    // 手牌区域平铺??
    [UdonBehaviourSyncMode(BehaviourSyncMode.None)]
    public class PickupUI : UdonSharpBehaviour
    {
        public bool hover;
        public bool up;
        public bool down;
        public bool cent;
        public void Enter() { hover = true; }//Debug.LogError("44"); }
        public void Exit() { hover = false; }//Debug.LogError("66"); }
        public void MUp() { up = true; }//Debug.LogError("1"); }
        public void MDown() { down = true; }//Debug.LogError("b"); }
        public void MC() { cent = true; }//Debug.LogError("c"); }
        public void MX() { up = down = cent = false; }//Debug.LogError("d"); }


        public bool kvalue(ref int hk)
        {
            if (hk == 0)
            {
                if (cent) hk = EX.MClick;
                if (up) hk = EX.UClick;
                if (down) hk = EX.DClick;
                return hk != 0;
            }
            else
            {
                if (!cent && !up && !down) hk = 0;
                return false;
            }
        }
    }
}