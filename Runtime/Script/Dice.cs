using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

namespace dev.ebi9.udoncard
{
    [UdonBehaviourSyncMode(BehaviourSyncMode.None)]
    public class Dice : UdonSharpBehaviour 
    {
        public Quaternion[] qs;

        public int nq = 0;

        private NetSystem ROOT; //同步对象 
        private void Start() => EX.init(nameof(NetSystem), ref ROOT);



        public void OnPlanePick()
        {
            nq = 0;
        }

        public void OnPlaneDrop()
        {
            if (!this.GetKinematic()) return;
            SendCustomEventDelayedSeconds(nameof(OnPlaneDrop2), 0.3f);
        }
        public void OnPlaneDrop2()
        {
            transform.rotation *= Random.rotation;
            ROOT.setTransform(name, transform);
            ROOT.setParent(this.name, ROOT.name);
            ROOT.setStatic(this.name, false); 
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (!Networking.IsMaster) return;
            if (collision.rigidbody != null) return;
            ROOT.setTransform(name, transform);
        }


        public void OnPlaneM1()
        {
            if (qs.Length == 0) return;
            nq = (nq + qs.Length - 1) % qs.Length;
            transform.rotation = qs[nq];
            ROOT.setLocalTransform(name, transform);
        }

        public void OnPlaneM2()
        {
            if (qs.Length == 0) return;
            nq = (nq + 1) % qs.Length;
            transform.rotation = qs[nq];
            ROOT.setLocalTransform(name, transform);
        }
    }
}