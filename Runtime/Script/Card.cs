using UdonSharp;
using UnityEngine;
using UnityEngine.UI;
using VRC.SDK3.Components;
using VRC.SDK3.Data;
using VRC.SDK3.Image;
using VRC.SDKBase;
using VRC.Udon;
using VRC.Udon.Common;
using VRC.Udon.Common.Interfaces;

namespace dev.ebi9.udoncard
{
    [UdonBehaviourSyncMode(BehaviourSyncMode.None)]
    public class Card : UdonSharpBehaviour
    {
        private NetSystem ROOT; //同步对象 

        public PerfabLoader pl;
        public Texture tex;
        public Vector2 off;
        public Vector2 sca;

        private void Start()
        {
            EX.init(nameof(NetSystem), ref ROOT);

            transform.parent.tryGetComponent(out pl);
        }

        public void OnPlanePick()
        {
            if (pl && pl.lx.Length == 0 && pl.HideIcon != null)
            {
                ROOT.SendNetworkEvent(this, nameof(updateimg), EX.localpid());
            }
        }

        public void OnPlaneDrop()
        {
            if (pl && pl.lx.Length == 0 && pl.HideIcon != null)
            {
                //updateimg(pid != 0 && EX.localpid() != pid);
                ROOT.SendNetworkEvent(this, nameof(updateimg), this.GetKinematic() ? EX.localpid() : 0);
            }
        }

        public void updateimg()
        {
            var pid = (int)ROOT.GetEventParam.Number;
            var hide = pid != 0 && EX.localpid() != pid;
            var main = GetComponentInChildren<Renderer>().material;
            if (main.mainTexture != pl.HideIcon)
            {
                if (tex != main.mainTexture)
                {
                    this.tex = main.mainTexture;
                    this.off = main.mainTextureScale;
                    this.sca = main.mainTextureOffset;
                }
            }
            main.mainTexture = hide ? pl.HideIcon : tex;
            main.mainTextureScale = hide ? Vector2.one : off;
            main.mainTextureOffset = hide ? Vector2.zero : sca;

        }


        public void OnPlaneM1()
        {
            if (EX.isvr())
            {
                OnPlaceMClick();
                return;
            }
            //横置
            var p = transform.position + (transform.GetKinematic() ? Vector3.zero : Vector3.up * 0.1f);
            var r = transform.rotation * Quaternion.AngleAxis(90, Vector3.forward);
            ROOT.setTransform(name, p, r.eulerAngles, PickupV3.移动速度);
        }

        public void OnPlaneM2()
        {
            if (EX.isvr())
            {
                OnPlaceMClick();
                return;
            }
            //横置
            var p = transform.position + (transform.GetKinematic() ? Vector3.zero : Vector3.up * 0.1f);
            var r = transform.rotation * Quaternion.AngleAxis(-90, Vector3.forward);
            ROOT.setTransform(name, p, r.eulerAngles, PickupV3.移动速度);
        }

        public void OnPlaceMClick()
        {
            //翻转
            var p = transform.position + (transform.GetKinematic() ? Vector3.zero : Vector3.up * 0.1f);
            var r = transform.rotation * Quaternion.AngleAxis(180, Vector3.down);
            ROOT.setTransform(name, p, r.eulerAngles, PickupV3.移动速度);
        }
    }
}