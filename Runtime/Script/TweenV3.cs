﻿using System;
using System.Collections.Generic;
using UdonSharp;
using UnityEngine;
using UnityEngine.UI;
using VRC.SDK3.Components;
using VRC.SDK3.Data;
using VRC.SDK3.Image;
using VRC.SDKBase;
using VRC.Udon;
using VRC.Udon.Common;
using VRC.Udon.Common.Interfaces;


namespace dev.ebi9.udoncard
{
    [UdonBehaviourSyncMode(BehaviourSyncMode.None)]
    public class TweenV3 : UdonSharpBehaviour
    {
        DataList tweens = new DataList();

        // param
        private const int P_TRAMSFORM = 0;
        private const int P_DURATION = 1;
        private const int P_TIME = 2;
        private const int P_TYPE = 3;
        private const int P_ARGS1 = 4;
        private const int P_ARGS2 = 5;
        private const int P_ARGS3 = 6;

        // type
        private const int T_MOVE = 1;
        private const int T_MOVE_LOCAL = 2;
        private const int T_ROTATE = 3;
        private const int T_ROTATE_LOCAL = 4;

        public void TweenMove(Transform obj, Vector3 pos, float t, string cb = "")
        {
            tweens.Add(new DataToken(new object[] {
                obj, t, 0f, T_MOVE, obj.position, pos, cb
            }));
        }

        public void TweenMoveLocal(Transform obj, Vector3 pos, float t, string cb = "")
        {
            tweens.Add(new DataToken(new object[] {
                obj, t, 0f, T_MOVE_LOCAL, obj.localPosition, pos, cb
            }));
        }

        public void TweenRotate(Transform obj, Quaternion pos, float t, string cb = "")
        {
            tweens.Add(new DataToken(new object[] {
                obj, t, 0f, T_ROTATE, obj.rotation, pos, cb
            }));
        }

        public void TweenRotateLocal(Transform obj, Quaternion pos, float t, string cb = "")
        {
            tweens.Add(new DataToken(new object[] {
                obj, t, 0f, T_ROTATE_LOCAL, obj.localRotation, pos, cb
            }));
        }


        private void Update()
        {
            for (int i = 0; i < tweens.Count;)
            {
                var tmp = (object[])tweens[i].Reference;
                var tf = (Transform)tmp[P_TRAMSFORM];
                var duration = (float)tmp[P_DURATION];
                var elapsedTime = (float)tmp[P_TIME];

                if (elapsedTime < duration && tf)
                {
                    // 计算当前时间比例
                    tmp[P_TIME] = elapsedTime += Time.deltaTime;
                    float t = elapsedTime / duration;

                    // 缓动函数
                    // liner only

                    // 使用插值函数实现平滑移动
                    switch (tmp[P_TYPE])
                    {
                        case T_MOVE:
                            tf.position = Vector3.Lerp((Vector3)tmp[P_ARGS1], (Vector3)tmp[P_ARGS2], t);
                            break;
                        case T_MOVE_LOCAL:
                            tf.localPosition = Vector3.Lerp((Vector3)tmp[P_ARGS1], (Vector3)tmp[P_ARGS2], t);
                            break;
                        case T_ROTATE:
                            tf.rotation = Quaternion.Lerp((Quaternion)tmp[P_ARGS1], (Quaternion)tmp[P_ARGS2], t);
                            break;
                        case T_ROTATE_LOCAL:
                            tf.localRotation = Quaternion.Lerp((Quaternion)tmp[P_ARGS1], (Quaternion)tmp[P_ARGS2], t);
                            break;
                    }

                    i++;
                }
                else
                {
                    tweens.RemoveAt(i);
                    var s = (string)tmp[P_ARGS3];
                    if (s != "" && tf)
                    {
                        tf.GetComponent<UdonBehaviour>().SendCustomEvent(s);
                    }
                }
            }
        }
    }
}