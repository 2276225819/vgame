Shader "Custom/ColorScreenBlendUnlitShader"
{
    Properties
    {
        _Color("Color", Color) = (1,1,1,1)
        _MainTex("Texture", 2D) = "white" {}
        _BumpMap("Normal Map", 2D) = "bump" {}
    }
        SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float2 uv2 : TEXCOORD1; // For normal map UV
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float2 uv2 : TEXCOORD1; // For normal map UV
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            sampler2D _BumpMap;
            float4 _Color;

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                o.uv2 = v.uv2; // Pass normal map UV
                return o;
            }

            half4 frag(v2f i) : SV_Target
            {
                half4 texColor = tex2D(_MainTex, i.uv);
                half4 color = _Color;
                half3 screenBlend = 1.0 - ((1.0 - texColor.rgb) * (1.0 - color.rgb));

                // Apply normal map
                half3 normal = tex2D(_BumpMap, i.uv2).rgb * 2.0 - 1.0; // Convert from [0,1] to [-1,1]
                normal = normalize(normal);

                // Here, just pass the blended color
                return half4(screenBlend, texColor.a);
            }
            ENDCG
        }
    }
        FallBack "Diffuse"
}