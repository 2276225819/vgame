
using UdonSharp;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using VRC.SDK3.Components;
using VRC.SDK3.Data;
using VRC.SDK3.Image;
using VRC.SDKBase;
using VRC.Udon;
using VRC.Udon.Common;
using VRC.Udon.Common.Interfaces;
using static VRC.SDKBase.VRCPlayerApi;

namespace dev.ebi9.udoncard
{
    [RequireComponent(typeof(VRC_Pickup))]
    [UdonBehaviourSyncMode(BehaviourSyncMode.Manual)]
    public class ManualObjectSync : UdonNetSharpBehaviour
    {
        [UdonSynced]
        public int currentHand;
        [UdonSynced]
        public int currentPlayerId;
        [UdonSynced]
        public Quaternion currentRota;
        [UdonSynced]
        public Vector3 currentPos;


        private void Update()
        {
            if (currentPlayerId == 0) return;
            if (!EX.GetPlayerById(currentPlayerId, out var player)) return;
            if (currentPlayerId != EX.localpid())
            {
                player.GetBoneData(currentHand, out var tt, out var rr);
                transform.position = tt + rr * currentPos;
                transform.rotation = rr * currentRota;
            }
            else
            {
                if (Input.GetMouseButtonUp(2))
                {
                    RequestSerialization(nameof(OnPickup2));
                }
            }
        }
        public override void OnPickup() => RequestSerialization(nameof(OnPickup2), 0.5f);//override 不支持 SendCustomEvent();
        public override void OnDrop() => RequestSerialization(nameof(OnDrop2));//override 不支持 SendCustomEvent();
        public override void OnDeserialization() => SendCustomEventDelayedSeconds(nameof(OnDeserialization2), 0.5f);

        public void OnPickup2()
        {
            if (!EX.local(out var player)) return;
            var p = GetComponent<VRC_Pickup>();
            if (p.currentHand == VRC_Pickup.PickupHand.None) return;
            var v = p.currentHand == VRC_Pickup.PickupHand.Left ? 1 : 2;
            currentHand = (v);
            currentPlayerId = player.playerId;


            player.GetBoneData(currentHand, out var tt, out var rr);
            currentRota = Quaternion.Inverse(rr) * transform.rotation;
            currentPos = Quaternion.Inverse(rr) * (transform.position - tt);
        }

        public void OnDrop2()
        {
            currentHand = 0; ;
            currentPlayerId = 0;
            currentPos = transform.position;
            currentRota = transform.rotation;
        }

        public override void OnPlayerLeft(VRCPlayerApi player)
        {
            if (!Networking.IsMaster) return;
            if (player.playerId != currentPlayerId) return;
            RequestSerialization(nameof(OnDrop2));
        }

        public void OnDeserialization2()
        {
            var pk = GetComponent<VRCPickup>();
            if (pk.DisallowTheft)
            {
                this.SetColliders(currentPlayerId == 0);
            }
            else if (pk.currentPlayer != null && pk.currentPlayer.playerId != currentPlayerId)
            {
                pk.Drop(pk.currentPlayer);
            }
            if (currentPlayerId == 0)
            {
                transform.position = currentPos;
                transform.rotation = currentRota;
            }
        }
    }

}