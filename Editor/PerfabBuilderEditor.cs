using UnityEngine;
using UnityEditor;
using UdonSharp;
using UnityEngine.UI;
using VRC.SDK3.Components;
using VRC.SDK3.Image;
using VRC.SDKBase;
using VRC.Udon;
using VRC.Udon.Common;
using VRC.Udon.Common.Interfaces;
using System.IO;
using System.Collections.Generic;
using VRC.SDK3.Data;
using dev.ebi9.udoncard;

namespace dev.ebi9.udoncard.Editor
{
    [CanEditMultipleObjects, CustomEditor(typeof(PerfabLoader), true)]
    public class PerfabBuilderEditor : UnityEditor.Editor
    {
        static int tabIndex;
        public override void OnInspectorGUI()
        {
            if (EditorApplication.isPlaying)
            {
                base.OnInspectorGUI(); 
                return;
            }

            var u = (PerfabLoader)target;
            if (GUILayout.Button("Card Data Editor"))
            {
                Application.OpenURL("https://ebi9.gitlab.io/stackedit/cardedit.html#" + u.CardDataUrl.Get());
            }
            if (GUILayout.Button("TTS Workshop Browser"))
            {
                Application.OpenURL("https://ebi9.gitlab.io/stackedit/ttsloader.html#");
            }
            if (GUILayout.Button("Reset Image Attribute"))
            {
                u.CardDataUrl = new VRCUrl("");
                u.ClsProps();
            }

            var os = u.CardDataUrl.ToString();
            base.OnInspectorGUI();
            var ns = u.CardDataUrl.ToString();
            u.CardDataUrl = new VRCUrl(ns);
            if (ns != "" && os != ns)
            {
                var urlhash = u.CardDataUrl.ToString();
                if (!urlhash.tryParseUrl(out string url, out string[] lx))
                {
                    u.ClsProps();
                }
                else if (lx.Length > 0)
                {
                    u.SetProps(lx[0]);
                }
                EditorUtility.SetDirty(target);
            }
        }


    }
}