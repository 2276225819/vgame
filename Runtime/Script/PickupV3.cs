using UdonSharp;
using UnityEngine;
using UnityEngine.UI;
using VRC.SDK3.Components;
using VRC.SDK3.Data;
using VRC.SDK3.Image;
using VRC.SDKBase;
using VRC.Udon;
using VRC.Udon.Common;
using VRC.Udon.Common.Interfaces;
using TMPro;
using UnityEngine.EventSystems;

namespace dev.ebi9.udoncard
{

    [RequireComponent(typeof(VRC_Pickup))]
    [UdonBehaviourSyncMode(BehaviourSyncMode.Manual)]

    public class PickupV3 : UdonSharpBehaviour
    {
        public const float 移动速度 = 0.2f;
        public float 卡牌长宽比 = 0.2f;
        public float 卡牌高度 = 0.003f;
        public string tableLayer = "Interactive";
        public string pickupLayer = "Pickup";
        [System.NonSerialized] public bool inpick;
        [System.NonSerialized] public bool intable;
        [System.NonSerialized] public bool lookVec;
        [System.NonSerialized] public RaycastHit tablepos;
        [System.NonSerialized] public RaycastHit ppp;
        [System.NonSerialized] public Vector3 tpos;
        [System.NonSerialized] public Vector3 tvec;
        [System.NonSerialized] public bool isvr;
        [System.NonSerialized] public int hk = EX.NClick;
        [System.NonSerialized] public Vector3 v;
        [System.NonSerialized] public float ln;
        [System.NonSerialized] public float cktime = 0;
        [System.NonSerialized] public Vector3 spos;
        [System.NonSerialized] public Vector3 hpos;
        [System.NonSerialized] public Vector3 urota = Vector3.zero;
        [System.NonSerialized] public Quaternion qrota = Quaternion.identity;
        [System.NonSerialized] public Collider lastpk;
        [System.NonSerialized] public float ahigh = 0; //释放距离
        float nowtime => Time.realtimeSinceStartup;


        [System.NonSerialized] public VRCPlayerApi player;
        [System.NonSerialized] public VRCPlayerApi.TrackingDataType TrackingType;
        [System.NonSerialized] protected DataList d = new DataList(); // <string,Collider>
        public PickupUI TMPUI;
        private LineRenderer[] lookln;
        private TextMeshPro Tip;
        private NetSystem Net;
        public Vector3 Rotate = Vector3.zero;


        [System.NonSerialized] public float h;
        [System.NonSerialized] public int state = 0;
        const int STATE_PICK = 0; //增加卡牌中  单击/按住:增加卡牌中   桌面单击:设置放位置  单击:扇形展开 
        const int STATE_FAN = 1; //展示扇形   桌面单击:设置放位置     空中单击:展示平铺
        const int STATE_TAIL = 2; //展示平铺   桌面单击:设置放位置     空中单击:增加卡牌中
        const int STATE_DROP = 3; //设置放位置 单击/桌面按住:立即放下 空中单击:增加卡牌中
        const int STATE_DRAG = 4; //设置放位置 单击/桌面按住:立即放下 空中单击:增加卡牌中

        Quaternion myrota => transform.rotation;


        protected void Start()
        {
            EX.init(nameof(NetSystem), ref Net);
            lookln = GetComponentsInChildren<LineRenderer>();
            Tip = GetComponentInChildren<TextMeshPro>();
            isvr = EX.isvr();
            if (EX.ismobile())
            {
                EX.init(nameof(PickupUI), ref TMPUI);
                TMPUI.GetComponent<Canvas>().enabled = false;
            }
            else
            {
                TMPUI = null;
            }
        }

        public override void OnAvatarEyeHeightChanged(VRCPlayerApi player, float prevEyeHeightAsMeters)
        {
            if (this.player == null || player == null || player.playerId != this.player.playerId) return;
            ahigh = Mathf.Max(1, prevEyeHeightAsMeters);
        }


        public void tableMove(Vector3 lpos, Vector3 svec, bool hold)
        {
            var up = Vector3.up;
            var dv = (Mathf.Abs(svec.x) > Mathf.Abs(svec.z)); //长宽
            h = transform.position.y - hpos.y; //高
            for (int i = 0; i < d.Count; i++)
            {
                var v1 = (Collider)(d[i].Reference);
                if (!v1)
                {
                    d.RemoveAt(i--);
                    continue;
                }
                var v2 = (v1).transform;
                var ii = 卡牌高度 * (i + 1);
                if (hold)
                {
                    // 固定角度平铺 
                    var zz = up * Mathf.Max(ii, h + ii);
                    var v = spos - lpos;
                    if (dv) xy(i, v.x, v.z, ref zz.x, ref zz.z);
                    else xy(i, v.z, v.x, ref zz.z, ref zz.x);
                    v2.position = spos + zz;
                    v2.rotation = intable ? (transform.rotation * qrota).RoundAngles() : transform.rotation * qrota;
                }
                else
                {
                    // 水平旋转自由旋转 
                    var zz = up * Mathf.Max(ii, h + ii + 卡牌长宽比 * (0.5f));
                    v2.position = lpos + zz; // 卡牌向上相对控制器位置
                    v2.rotation = transform.rotation * qrota; // 卡牌相对控制器角度
                }
            }
        }

        public void handHold(bool lastone = false)
        {
            var up = transform.rotation * urota;
            for (int i = lastone ? d.Count - 1 : 0; i < d.Count; i++)
            {
                // 位置固定放置手上
                var v1 = (Collider)(d[i].Reference);
                if (!v1)
                {
                    d.RemoveAt(i--);
                    continue;
                }
                var v2 = (v1).transform;
                var ii = 卡牌高度 * 0.1f * (i) + .1f;


                // 水平旋转自由旋转  
                v2.rotation = transform.rotation * qrota; // 卡牌相对控制器角度
                v2.position = transform.position + up * ii; // 卡牌向上相对控制器位置
                Net.setLocalTransform(v2.name, v2.transform, 移动速度);
            }
        }

        public void handMove(Vector3 lpos, bool drag)
        {
            var l = 24;
            var mx = (d.Count / l) * l;
            var md = d.Count % l;
            ln = 5; // v.magnitude * 伞形展开比;
            for (int i = 0; i < d.Count; i++)
            {
                var v1 = (Collider)(d[i].Reference);
                if (!v1)
                {
                    d.RemoveAt(i--);
                    continue;
                }
                var v2 = (v1).transform;
                var ii = 卡牌高度 * 0.1f * (i + 1);

                if (drag)
                {
                    v2.rotation = transform.rotation;
                    v2.position = Vector3.Lerp(spos, lpos, (float)i / d.Count) +
                                  v2.rotation * new Vector3(0, 卡牌长宽比, ii);
                }
                else
                {
                    // 伞形展开比
                    var rrr = transform.rotation * Quaternion.Euler(Rotate);
                    var vc = i >= mx ? md : l;
                    var vi = i % l;
                    v2.rotation = rrr * Quaternion.Euler(0, 0, (vi - vc / 2) * ln);
                    var zz = rrr * new Vector3(0, 卡牌长宽比 * 0.5f * (-2 + i / l), -卡牌高度 * (i / l * 5)) +
                             v2.rotation * new Vector3(0, 卡牌长宽比 * 2, ii);
                    v2.position = lpos + zz;
                }
            }
        }

        protected void xy(int i, float vx, float vz, ref float zx, ref float zy)
        {
            var ln = (int)(vx / 卡牌长宽比) + (vx > 0 ? 1 : -1);
            var rw = vz / (d.Count / (float)ln);
            zx -= 卡牌长宽比 * (int)(i % ln) * (ln > 0 ? 1 : -1);
            zy -= rw * (int)(i / ln);
        }


        void dopkadd(Collider pk, bool parent = true, bool hold = true)
        {
            if (pk == null) return;
            if (d.Contains(pk)) return;
            d.Add(pk);
            pk.SetKinematic(true);
            pk.SetColliders(false);
            pk.SendCustomEvent(nameof(Card.OnPlanePick));
            if (!parent) return;
            pk.transform.parent = transform;
            Net.setParent(pk.name, this.name);
            Net.setStatic(pk.name, true);
            if (!hold) return;
            handHold(true);
        }

        void dopkdel(bool parent, bool fixpos)
        {
            // 1锁定到父级    2锁定到空中   3原地放下
            for (int i = 0; i < d.Count; i++)
            {
                var v1 = ((Collider)(d[i].Reference));
                if (!v1)
                {
                    d.RemoveAt(i--);
                    continue;
                }
                v1.SetColliders(true);
                v1.SetKinematic(fixpos);
                v1.SendCustomEvent(nameof(Card.OnPlaneDrop));
                Net.setTransform(v1.name, v1.transform, 移动速度);
                Net.setParent(v1.name, parent ? this.name : Net.name);
                Net.setStatic(v1.name, fixpos);
            }

            d.Clear();
        }


        public override void OnPickup()
        {
            if (!EX.local(out player)) return;
            var p = GetComponent<VRC_Pickup>();
            if (isvr)
            {
                if (p.currentHand == VRC_Pickup.PickupHand.Left)
                    TrackingType = VRCPlayerApi.TrackingDataType.LeftHand;
                if (p.currentHand == VRC_Pickup.PickupHand.Right)
                    TrackingType = VRCPlayerApi.TrackingDataType.RightHand;
            }
            else
            {
                TrackingType = VRCPlayerApi.TrackingDataType.Head;
            }

            state = STATE_PICK;
            for (int i = 0; i < transform.childCount; i++)
            {
                var pk = transform.GetChild(i).GetComponent<Collider>();
                if (pk == null) continue;
                dopkadd(pk, true, false);
            }

            if (TMPUI) TMPUI.GetComponent<Canvas>().enabled = true;
            ahigh = Mathf.Max(1, player.GetAvatarEyeHeightAsMeters());
        }

        public override void OnDrop()
        {
            TrackingType = VRCPlayerApi.TrackingDataType.Origin;
            player = null;
            dopkdel(true, true);
            if (TMPUI) TMPUI.GetComponent<Canvas>().enabled = false;
        }

        public override void OnPlayerLeft(VRCPlayerApi player)
        {
            if (this.player == null) return;
            if (this.player.playerId != player.playerId) return;
            OnDrop();
        }


        public override void OnPickupUseDown()
        {
            if (TMPUI && TMPUI.hover) return;
            if (!lookVec) return;
            cktime = nowtime;
            spos = (intable ? tablepos.point : transform.position);
            hpos = transform.position;
            if (d.Count == 0 && inpick)
            {
                var q = ppp.collider.transform;
                urota = Quaternion.Inverse(transform.rotation) * Vector3.up; // 上向量
                qrota = Quaternion.Inverse(transform.rotation) * q.rotation; //卡牌和手相差角度 
                lastpk = ppp.collider;
            }
        }

        public override void OnPickupUseUp()
        {
            if (TMPUI && TMPUI.hover) return;
            if (!lookVec) return;
            var lpos = inpick ? ppp.point : (intable ? tablepos.point : tpos + tvec);
            var hold = cktime != 0 && nowtime - cktime > 0.5f;
            // 特殊操作   
            if (d.Count == 0)
            {
                if (inpick)
                {
                    state = STATE_PICK;
                    dopkadd(ppp.collider);
                }
            }
            else if (state == STATE_PICK && !hold)
            {
                if (inpick)
                {
                    dopkadd(ppp.collider);
                }
                else if (!intable)
                {
                    state = STATE_FAN;
                    handMove(spos = transform.position, false);
                }
                else
                {
                    state = STATE_DROP;
                    tableMove(lpos, tvec, false);
                }
                //dosync();
            }
            else if (state == STATE_FAN)
            {
                dopkdel(false, true);
            }
            else if (state == STATE_DROP || state == STATE_DRAG)
            {
                dopkdel(false, false);
            }

            cktime = 0;
            lastpk = null;
            Net.SendNetworkEvent(this, nameof(updateNum), new DataToken());
        }
        public void updateNum()
        {
            Tip.text = $"{GetComponentsInChildren<Collider>().Length - 1}";
        }

        private void Update()
        {
            if (!(lookVec = EX.lookVec(player, TrackingType, ref tpos, ref tvec, out var trota))) return;
            intable = EX.Raycast(tableLayer, tpos, tvec, ahigh * 2f, out tablepos);
            inpick = EX.Raycast(pickupLayer, tpos, tvec, intable ? ahigh * 2f : ahigh * 0.6f, out ppp);
            var hold = cktime != 0 && nowtime - cktime > 0.5f;
            var lpos = (intable ? tablepos.point : transform.position);
            var move = (spos - lpos).magnitude > 0.01f;
            if (d.Count == 0)
            {
                if (cktime != 0 && lastpk && inpick)
                {
                    if (move && intable)
                    {
                        state = STATE_DRAG;
                        cktime = nowtime;
                        dopkadd(lastpk, false);
                    }
                    else if (hold)
                    {
                        state = STATE_PICK;
                        dopkadd(lastpk);
                    }
                }
            }
            else if (state == STATE_PICK)
            {
                if (cktime != 0 && inpick)
                {
                    if (hold || move)
                    {
                        dopkadd(ppp.collider);
                    }
                }
            }
            else if (state == STATE_FAN)
            {
                if (cktime == 0 && intable)
                {
                    state = STATE_PICK;
                    handHold();
                }

                if (cktime != 0 && hold)
                {
                    handMove(transform.position, hold);
                }
            }
            else if (state == STATE_DROP)
            {
                if (cktime == 0 && !intable)
                {
                    state = STATE_PICK;
                    handHold();
                }
                else
                {
                    tableMove(inpick ? ppp.point : lpos, tvec, hold);
                }
            }
            else if (state == STATE_DRAG)
            {
                var v1 = (Collider)(d[0].Reference);
                if (!v1)
                {
                    d.RemoveAt(0);
                    return;
                }
                var v2 = (v1).transform;
                var ii = h + 卡牌高度 * (0 + 1);
                var zz = Vector3.up * ii;
                lpos.y = v2.position.y;
                v2.position = lpos;
            }

            // 指示器
            for (int i = 0; i < lookln.Length; i++)
            {
                var lk = lookln[i];
                if (inpick && (state == STATE_PICK || d.Count == 0))
                {
                    //lk.SetPosition(1, ppp.point + ppp.normal * 0.07f);
                    //lk.SetPosition(0, ppp.point + ppp.normal * 0.01f);
                    lk.SetPosition(1, ppp.point);
                    lk.SetPosition(0, transform.position);
                }
                else if (intable && d.Count != 0)
                {
                    //lk.SetPosition(0, lpos + Vector3.up * 0.07f);
                    //lk.SetPosition(1, lpos + Vector3.up * 0.01f);
                    lk.SetPosition(1, lpos);
                    lk.SetPosition(0, transform.position);
                }
                else
                {
                    lk.SetPosition(0, Vector3.down * 10);
                    lk.SetPosition(1, Vector3.down * 10);
                }
            }

            // 功能键
            if (TMPUI ? TMPUI.kvalue(ref hk) : kvalue(ref hk))
            {
                if (d.Count == 0)
                {
                    if (inpick && ppp.collider.tryGetComponent(out UdonBehaviour pk1))
                    {
                        if (hk == EX.MClick) pk1.SendCustomEvent(nameof(Card.OnPlaceMClick));
                        if (hk == EX.UClick) pk1.SendCustomEvent(nameof(Card.OnPlaneM1));
                        if (hk == EX.DClick) pk1.SendCustomEvent(nameof(Card.OnPlaneM2));
                    }
                }
                else if (state == STATE_PICK || state == STATE_DROP)
                {
                    if (hk == EX.MClick || hk == EX.UClick || hk == EX.DClick)
                    {
                        qrota *= Quaternion.AngleAxis(180, Vector3.down);
                        if (state == STATE_PICK) handHold();
                    }
                }
            }
        }


        float st;

        bool kvalue(ref int hk)
        {
            var md = Input.GetMouseButton(2);
            var v = Input.GetAxis(isvr ? "Oculus_CrossPlatform_SecondaryThumbstickVertical" : "Mouse ScrollWheel");
            if (hk == 0)
            {
                if (md) hk = EX.MClick;
                if (v > 0) hk = EX.DClick;
                if (v < 0) hk = EX.UClick;
                if (hk != 0)
                {
                    st = nowtime;
                }

                return false;
            }
            else
            {
                if (v == 0 && !md)
                {
                    if (st != 0 && nowtime - st < 0.5f)
                    {
                        st = 0;
                        return true;
                    }

                    hk = 0;
                }

                return false;
            }
        }
    }
}